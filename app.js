var express = require('express');
var cors = require('cors');
var app = express();

app.use(cors());

// sol bölümde gözükecek listeye id ve name bilgilerinin mongodb'den çekilmesi için
app.post('/getData', function(req, res, next) {
    const MongoClient = require('mongodb').MongoClient;
    const uri = "mongodb+srv://dbUser:dbPassword@cluster0-6ehlt.mongodb.net/?ssl=true&authSource=admin";
    const client = new MongoClient(uri, { useNewUrlParser: true,useUnifiedTopology: true });
    client.connect(err => {
    const collection = client.db("sample_airbnb").collection("listingsAndReviews");
      collection.find({}).project({ _id : 1, name : 1 }).toArray(function(err, result) {
        res.json(result);
      });
      
    });
});

// listedeki herhangi bir veriye tıklanıldığında onun id'sinden verilerin çekilmesi için 
app.post('/getData/:id', function(req, res, next) {
  var id = req.params.id;
  const MongoClient = require('mongodb').MongoClient;
  const uri = "mongodb+srv://dbUser:dbPassword@cluster0-6ehlt.mongodb.net/?ssl=true&authSource=admin";
  const client = new MongoClient(uri, { useNewUrlParser: true,useUnifiedTopology: true });
  client.connect(err => {
  const collection = client.db("sample_airbnb").collection("listingsAndReviews");
    collection.findOne({_id:id},function(err, result) {
      res.json(result);
    });
  });
});

// search kısmına yazılan kelime sayesinde liste güncellemesi için mongodb'den id ve name bilgilerinin çekilmesi için
app.post('/search/:word', function(req, res, next) {
  var word = req.params.word;
  console.log(word);
  const MongoClient = require('mongodb').MongoClient;
  const uri = "mongodb+srv://dbUser:dbPassword@cluster0-6ehlt.mongodb.net/?ssl=true&authSource=admin";
  const client = new MongoClient(uri, { useNewUrlParser: true,useUnifiedTopology: true });
  client.connect(err => {
  const collection = client.db("sample_airbnb").collection("listingsAndReviews");
    collection.find({name: { '$regex': word}}).project({ _id : 1, name : 1 }).toArray(function(err, result) {
      res.json(result);
    });
  });
});

app.listen(3000, function() {
    console.log('listening on 3000')
})