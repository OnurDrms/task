import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GetdataService {

  constructor(
    private http: HttpClient
  ) { }

  // mongodb'den liste için verilerin alınması
  getAllDatas(){
    return this.http.post('http://127.0.0.1:3000/getData',{});
  }

  // listeden seçilecek verinin id bilgisiyle verilerin alınması
  getOneData(id){
    return this.http.post('http://127.0.0.1:3000/getData/'+id,{});
  }

  // arama kısmında yazılan kelime ile id ve name bilgilerinin alınması
  getSearchData(word){
    return this.http.post('http://127.0.0.1:3000/search/'+word,{});
  }
}
