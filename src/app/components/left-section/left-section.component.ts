import { Component, OnInit ,Output, EventEmitter, Input, OnChanges} from '@angular/core';
import { GetdataService } from '../../services/getdata.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-left-section',
  templateUrl: './left-section.component.html',
  styleUrls: ['./left-section.component.css']
})
export class LeftSectionComponent implements OnInit,OnChanges {

    datas = []
    @Output() send = new EventEmitter(); // listede tıklanılanın bilgisini parent'a göndermek için
    @Input() searchData; // parent'tan aramadan gelen kayıtları almak için

    constructor(
      private dataService: GetdataService,
      private SpinnerService: NgxSpinnerService
    ) { }
    ngOnInit(): void {
      
    }
    ngOnChanges() {
      this.getAll();
    }
    
    // parent'tan alınan arama bilgisi varsa bunu listeye yansıtma yoksa tüm verileri listeleme
    getAll(){
      if(this.searchData.length>0){
        this.datas = [];
        this.SpinnerService.show();  
        this.datas.push(this.searchData);
        this.SpinnerService.hide();     
      }else{
        this.SpinnerService.show();  
        this.dataService.getAllDatas().subscribe((res) =>{
          this.datas.push(res);
          this.SpinnerService.hide();     
        });
      }
    }

    // listede tıklanılanın id'sini parent componente gönderme
    clickFunction(id: number){
      if(id){
        this.send.emit({ _id: id });
      }
    }
}
