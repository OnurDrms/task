import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { GetdataService } from 'src/app/services/getdata.service';
import { NgxSpinnerService } from "ngx-spinner";  

@Component({
  selector: 'app-mid-section',
  templateUrl: './mid-section.component.html',
  styleUrls: ['./mid-section.component.css']
})
export class MidSectionComponent implements OnInit,OnChanges {
  value = {};
  show = false;

  // parent'tan listede secilenin id bilgisini almak için
  @Input() data: number; 

  constructor(
    private dataService: GetdataService,
    private SpinnerService: NgxSpinnerService
  ) { }

  ngOnInit(): void {
   
  }

  // id bilgisi varsa service aracılığıyla verilerin çekilmesi için
  ngOnChanges(){ 
    if(this.data > 0){
      this.SpinnerService.show();
      this.dataService.getOneData(this.data).subscribe((res) =>{ 
        if(res){
          this.value = res;
          this.show = true;
        }
        this.SpinnerService.hide(); 
      });
    }  
  }
}
