import { Component, OnInit, Output, EventEmitter, OnChanges } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  id = 0;
  searchValue = [];
  
  constructor() { }

  ngOnInit(): void {
   
  }
  // listede tıklanılanın id'sinin mid-componente gönderilmesi için
  click(data: { _id: number; }) {
    if(data){
      this.id = data._id;
    }
  }

  // arama kısmından veriyi alıp left-section kısmını güncelleme için
  searchClick(data: { _word: any[]; }) {
    this.searchValue = data._word;
  }
}
