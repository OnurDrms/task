import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { GetdataService } from 'src/app/services/getdata.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  // parent componente arananı göndermek için
  @Output() search = new EventEmitter(); 

  constructor(
    private dataService: GetdataService,
    private SpinnerService: NgxSpinnerService
  ) { }

  ngOnInit(): void {
  }

  // arama kısmında bişey yazılmadıysa ve yazıldıysa yapılacaklar 
  searchValue(word: string){
    if(word == ""){
      this.SpinnerService.show();
      this.dataService.getAllDatas().subscribe((res) =>{
        this.search.emit({ _word: res }); 
        this.SpinnerService.hide(); 
      });
    }else{
      this.SpinnerService.show();
      this.dataService.getSearchData(word).subscribe((res) =>{  
        this.search.emit({ _word: res });
        this.SpinnerService.hide();
      });
    }
  }

  // arama kısmında enter'a basıldıgında servis aracılığıyla yapılacaklar
  keyPress(e: { keyCode: number; },searchValue: any){
    if (e.keyCode == 13 && searchValue != "") {
      this.SpinnerService.show();
      this.dataService.getSearchData(searchValue).subscribe((res) =>{  
        this.search.emit({ _word: res });
        this.SpinnerService.hide();
      });
    }
  }
}
